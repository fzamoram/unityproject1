using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class InputActionPlayer : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_Event;


    public Animator animator;

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;

    [SerializeField]
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_SpriteRenderer;
    
    

    [SerializeField]
    private float m_MovementSpeed = 3f;

    [SerializeField]
    private LayerMask m_ActionLayerMask;

    private float m_Life = 3;

    [SerializeField]
    private AudioClip m_AudioClip;

    [SerializeField]
    private Waves m_Waves;


    void Awake()
    {
        this.GetComponent<ParticleSystem>().Stop();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("Movement").FindAction("Move");
        m_Input.FindActionMap("Movement").Enable();
    }


    Vector2 m_Movement = Vector2.zero;
    void Update()
    {
        m_Movement = m_MovementAction.ReadValue<Vector2>();

        animator.SetFloat("Horizontal", m_Movement.x);
        animator.SetFloat("Vertical", m_Movement.y);
        animator.SetFloat("Speed", m_Movement.sqrMagnitude);
    }
    private void FixedUpdate()
    {
        m_Rigidbody.velocity = m_Movement * m_MovementSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.attachedRigidbody.gameObject.layer == 4)
        {
            Debug.Log("Agua");
            m_SpriteRenderer.color = Color.blue.WithAlpha(0.2f);
        }

        if (collision.attachedRigidbody.gameObject.tag == "Danger")
        {
            AudioSource.PlayClipAtPoint(m_AudioClip, Camera.main.transform.position);

            this.GetComponent<ParticleSystem>().Play();
            Debug.Log(m_Life);
            float alpha = 1 - (m_Life / 5f);
            Debug.Log(alpha);
            m_SpriteRenderer.color = Color.green.WithAlpha(alpha);
            m_Life--;
            if (m_Life <= 0)
            {
                if(m_Waves.spd > m_Waves.MaxSpd)
                {
                    m_Waves.MaxSpd = m_Waves.spd;
                }
                m_Event.Raise();
                GameManager.Instance.ChangeScene(GameManager.GameOverScene);
            }
        }
    }


}

