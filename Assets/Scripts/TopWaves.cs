using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TopWaves : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI m_MaxScoreText;

    [SerializeField]
    Waves waves;
    private void Awake()
    {
        m_MaxScoreText.text = "Max Waves Lasted: "+waves.maxWaves;
    }
}
