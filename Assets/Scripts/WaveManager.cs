using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    int wave;
    [SerializeField]
    Spawner spawner;

    [SerializeField]
    Waves waves;
    // Start is called before the first frame update
    void Start()
    {
        wave = 1;
        waves.wavesCount = 1;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Wave: " + wave;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void ChangeWave()
    {
        wave++;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Wave: " + wave;
    }

    private void OnEnable()
    {
        if (spawner)
            spawner.nWave += ChangeWave;
    }

    private void OnDisable()
    {
        if (spawner)
            spawner.nWave -= ChangeWave;
    }

    public void mierda()
    {
        waves.wavesCount = wave;
        if (waves.wavesCount> waves.maxWaves)
        {
            waves.maxWaves = waves.wavesCount;
        }
    }
}
