using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

[CreateAssetMenu]
public class Waves : ScriptableObject
{
    public int wavesCount = 0;
    public int maxWaves = 0;
    public float spd = 1f;
    public float MaxSpd = 1f;

}
