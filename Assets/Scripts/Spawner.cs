using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject Pool;

    [SerializeField]
    float random1x;
    [SerializeField]
    float random2x;

    [SerializeField]
    float random1y;
    [SerializeField]
    float random2y;

    [SerializeField]
    float randomPosX1;
    [SerializeField]
    float randomPosX2;
    [SerializeField]
    float randomPosY1;
    [SerializeField]
    float randomPosY2;

    [SerializeField]
    private Waves m_Waves;

    public delegate void Wave();
    public event Wave nWave;

    void Start()
    {
        StartCoroutine(Spawn());
        m_Waves.spd = 1f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator Spawn()
    {
        int veces = 0;
        while (true)
        {
            for (int i = 0; i < Pool.transform.childCount; i++)
            {
                Transform tActual = Pool.transform.GetChild(i);
                
                if (tActual.gameObject.active == false)
                {
                    tActual.gameObject.SetActive(true);
                    tActual.position = new Vector2 (Random.Range(randomPosX1, randomPosX2), Random.Range(randomPosY1, randomPosY2));
                    tActual.GetComponent<Rigidbody2D>().velocity = new Vector2 (Random.Range(random1x,random2x)*m_Waves.spd, Random.Range(random1y, random2y) * m_Waves.spd);
                    veces++;
                    break;
                }
            }
            if (veces == 10)
            {
                veces = 0;
                nWave?.Invoke();
                m_Waves.spd += 0.5f;
                yield return new WaitForSeconds(10);
            }
            yield return new WaitForSeconds(2);
        }

    }
}
