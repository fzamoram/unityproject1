using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Windows;

public class ButtonScript : MonoBehaviour
{
    public void MainMenu()
    {
        GameManager.Instance.ChangeScene("MainMenu");
    }
    public void Play()
    {
        GameManager.Instance.ChangeScene("Game");
    }
    public void Quit()
    {
        Application.Quit();
    }
}
