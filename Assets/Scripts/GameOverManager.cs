using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_Event;

    void Awake()
    {
        m_Event.Raise();
    }
}
