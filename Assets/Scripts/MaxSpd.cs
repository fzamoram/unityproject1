using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MaxSpd : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI m_MaxSpdText;
    [SerializeField]
    Waves m_Waves;
    private void Awake()
    {
        m_MaxSpdText.text = "Max Speed: " + m_Waves.MaxSpd;
    }
}
