using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;

    public const string PlayScene = "Game";
    public const string MenuScene = "MainMenu";
    public const string GameOverScene = "GameOver";

    private int m_Waves = 0;
    public int TopWaves
    {
        get
        {
            return m_Waves;
        }
        set
        {
            if (value > m_Waves)
                m_Waves = value;
        }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitValues();

        ChangeScene(MenuScene);
    }

    //Es crida al carregar una escena
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("GameManager - OnSceneLoaded: " + scene.name);
    }

    private void InitValues()
    {
        m_Waves = 0;
        //resetejar la dificultat, etc
    }

    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}

