using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Actual : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI m_MaxSpdText;
    [SerializeField]
    Waves m_Waves;
    private void Awake()
    {
        m_MaxSpdText.text = "Actual: "+m_Waves.wavesCount+" Waves  Speed: "+m_Waves.spd;
    }
}
